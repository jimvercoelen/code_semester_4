package woordenapplicatie.gui;


import java.net.URL;
import java.util.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

/**
 * FXML Controller class
 *
 * @author Bas van Zutphen
 */
public class WoordenController implements Initializable
{
    private static final String DEFAULT_TEXT =   "Een, twee, drie, vier\n" +
         "Hoedje van, hoedje van\n" +
         "Een, twee, drie, vier\n" +
         "Hoedje van papier\n" +
         "\n" +
         "Heb je dan geen hoedje meer\n" +
         "Maak er één van bordpapier\n" +
         "Eén, twee, drie, vier\n" +
         "Hoedje van papier\n" +
         "\n" +
         "Een, twee, drie, vier\n" +
         "Hoedje van, hoedje van\n" +
         "Een, twee, drie, vier\n" +
         "Hoedje van papier\n" +
         "\n" +
         "En als het hoedje dan niet past\n" +
         "Zetten we 't in de glazenkas\n" +
         "Een, twee, drie, vier\n" +
         "Hoedje van papier";

    private List<String> wordList = new LinkedList<String>();
    private List<String> sentenceList = new LinkedList<String>();

 @Override
 public void initialize(URL location, ResourceBundle resources)
 {
     List<String> tempList = new LinkedList<String>();

     taInput.setText(DEFAULT_TEXT);
     tempList = splitWords(DEFAULT_TEXT);

     for (String s : tempList)
     {
         // De string normalizen ( http://stackoverflow.com/questions/3322152/is-there-a-way-to-get-rid-of-accents-and-convert-a-whole-string-to-regular-lette )
         s = s.replaceAll("[^\\p{ASCII}]", "");

         // Alle eerste ( eventueel hoofd-)letters omzetten in kleine letters
         s = s.substring(0, 1).toLowerCase() + s.substring(1);


         // daarnaast staan er nog losse letters in de tempList. Ik heb besloten om losse letters niet als woord te laten tellen.
         if (s.length() != 1 ) { wordList.add(s); }

         System.out.println(s);
     }

 }
    
    @FXML
    private Button btAantal;

    @FXML
    private TextArea taInput;

    @FXML
    private Button btSorteer;

    @FXML
    private Button btFrequentie;

    @FXML
    private Button btConcordantie;

    @FXML
    private TextArea taOutput;


    
    @FXML
    /**
     * De methode aantalAction telt het aantal woorden & het aantal unieke woorden.
     * De methode is best straight-forward.
     */

    private void aantalAction(ActionEvent event)
    {
        taOutput.clear();

        // in een set kunnen geen dubbelen zitten --> elk element is uniek.
        Set<String> uniqueList = new TreeSet<String>();
        uniqueList.addAll(wordList);

        int uniqueWords = uniqueList.size();
        int totalWords = wordList.size();

        taOutput.appendText("Totaal aantal woorden: " + totalWords + "\n");
        taOutput.appendText("Totaal aantal unieke woorden: " + uniqueWords);
    }

    @FXML
    /**
     * De methode sorteerAction sorteert de woorden in een omgekeerde alfabetische volgorde.
     * Ik heb alle trema's , hoofdletters en andere leestekens omgezet zodat er één reeks ( van z tot a ) woorden gesorteerd kan worden.
     * ( Indien ik dat niet zo doen heb je eerst een reeks van Z tot A, vervolgens van z tot a en tot slot nog een reeks met trema-woorden. )
     */

    private void sorteerAction(ActionEvent event)
    {
        taOutput.clear();

        // eerst in een HashSet zetten om de dubbelen eruit te halen
        Set<String> uniqueList = new HashSet<String>();
        uniqueList.addAll(wordList);

        // Vervolgens in een ArrayList<String> zetten zodat de reverseOrder() kan worden toegepast.
        ArrayList<String> sorteerList = new ArrayList<String>();
        sorteerList.addAll(uniqueList);
        Collections.sort(sorteerList, Collections.reverseOrder());


        for (String s: sorteerList)
        {
            taOutput.appendText(s + "\n");
        }


    }

    @FXML
    /**
     * De methode frequentieAction houdt per woordt bij hoevaak een bepaald woord
     * voorkomt in het versje. Ik heb nog moeite gedaan om
     * de values ( in dit geval het aantal keer dat een woord voorkomt) te sorteren
     * van meest voorkomend naar minst voorkomend. helaas is dit niet gelukt :(
     */

    private void frequentieAction(ActionEvent event)
    {
        taOutput.clear();

        Map<String, Integer> wordCount = new HashMap<String, Integer>();

        for(String word: wordList)
        {
            Integer count = wordCount.get(word);
            wordCount.put(word, (count==null) ? 1 : count+1);
        }

        for (String key : wordCount.keySet())
        {
            taOutput.appendText(key + ": " + wordCount.get(key) + "\n");
        }

    }

    @FXML
    /**
     * De methode concordatieAction houdt per woordt bij op welke regel ze staan.
     * Gezien de niet erg efficiënte manier van programmeren van deze methode,
     * heb ik inline nog wat commentaar gezet.
     */

    private void concordatieAction(ActionEvent event)
    {
        taOutput.clear();
        sentenceList = splitSentences(DEFAULT_TEXT);

        Map<String, ArrayList<Integer>> concordanceMap = new HashMap<String, ArrayList<Integer>>();

        for ( int i = 0 ; i <wordList.size(); i++)
        {

            // De arraylist waarin de posities van elk woord worden bijgehouden.
            ArrayList<Integer> positions = new ArrayList<>();

            // Simpele iterator die bijhoudt bij welke zin het woord ( mogelijk ) is gevonden
            int sentenceCounter = 1;

            // Voor elke zin in de zinnenlijst...
            for (String s : sentenceList)
            {
                //  als de zin een woord uit de woordenlijst bevat dan...
                if ( s.contains(wordList.get(i)))
                {
                    // sla de positie op in de positielijst en...
                    positions.add(sentenceCounter);

                    // sla de key ( het woord ) op met de lijst van posities.
                    concordanceMap.put(wordList.get(i), positions );
                }
                // ophogen zinnenteller
                sentenceCounter++;
            }
        }

        // itereren door de hashmap en printen van de key + values.
        // in dit geval: Woord + regelposities
        for (Map.Entry<String,ArrayList<Integer>> entry : concordanceMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue().toString();

            taOutput.appendText(key + " : " + value + "\n");
        }
    }
    /**
     * De methode splitWords zet het versje om in losse woorden.
     * @param versje is het rijmpje.
     */

    private List<String> splitWords(String versje)
    {
        List<String> tempList = new LinkedList<String>();
        String tempWord = "";

        for (int i = 0; i < versje.length() ; i++)
        {
            String c = Character.toString(versje.charAt(i));

            if (c.equals(",") || c.equals(".") || c.equals(" ") || c.equals("\n") || c.equals("'"))
            {
                if (tempWord != "")
                {
                    tempList.add(tempWord);
                }
                tempWord = "";
            }
            else
            {
                tempWord = tempWord + c;
                // indien het om de laaste letter van het laatste woord gaat én het is geen delimiter --> voeg het woord toe.
                if (i == versje.length() - 1)
                {
                    tempList.add(tempWord);
                }
            }
        }
        return tempList;
    }

    /**
     * De methode splitWords zet het versje om in losse zinnen.
     * @param versje is het rijmpje.
     */
    private List<String> splitSentences(String versje)
    {

        List<String> tempList = new LinkedList<>();
        String tempSentence = "";

        for (int i = 0; i < versje.length() ; i++)
        {
            String c = Character.toString(versje.charAt(i));

            if (c.equals("\n"))
            {
                if (tempSentence != "")
                {
                    tempList.add(tempSentence);
                }
                tempSentence = "";
            }
            else
            {
                tempSentence = tempSentence + c;
                // indien het om de laaste letter van de laatste zin gaat, en het is geen delimiter --> voeg de zin toe.
                if (i == versje.length() - 1)
                {
                    tempList.add(tempSentence);
                }
            }
        }
        return tempList;

    }
}
