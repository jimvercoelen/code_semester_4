package com.jim;

import com.jim.util.Reader;
import com.jim.util.Writer;

import java.io.Serializable;
import java.util.*;

public class Main {

    private static final String FIELD_OFFSET = "";
    private static final String FIELD_FORMAT = "%-1s%s%n";
    private Scanner scanner = new Scanner(System.in);

    public static class Node implements Comparable<Node>, Serializable {
        private Character character;
        private int frequency;
        private Node leftChild;
        private Node rightChild;

        Node(char character, int frequency) {
            this.character = character;
            this.frequency = frequency;
        }

        Node(Node leftChild, Node rightChild) {
            this.frequency = leftChild.frequency + rightChild.frequency;
            this.leftChild = leftChild;
            this.rightChild = rightChild;
        }

        boolean isLeaf() {
            return  (leftChild == null) && (rightChild == null);
        }

        /**
         * Order:
         *  - Ascending order (1, 2, 3, 4, ...)
         *  - Alphabetically (a, b, c, d, ...)
         *
         * Note: null checks required because of the characters (they are nullable)!
         */
        @Override
        public int compareTo(Node o) {
            return frequency < o.frequency ? -1
                    : frequency > o.frequency ? +1
                    : character == null && o.character == null ? leftChild.compareTo(o.leftChild)   // <-- left or right doesn't matter
                    : character == null ? - 1
                    : o.character == null ? 1
                    : character.compareTo(o.character);
        }
    }

    private void compress() {
        // Get user input
        String input = getUserInput("Absolute path to .txt file: ");

        // Read text file
        String text = Reader.readTxtFile(input);

        // Print text
        System.out.println("Text:");
        System.out.println(text);

        // Get frequency for each character
        Map<Character, Integer> frequencyMap = new HashMap<>();
        for (char character: text.toCharArray()) {
            frequencyMap.put(character, frequencyMap.getOrDefault(character, 0) + 1);
        }

        // Create node set
        // (This set will automatically be sorted thanks to Node.compareTo)
        TreeSet<Node> nodes = new TreeSet<>();
        for (Map.Entry<Character, Integer> entry : frequencyMap.entrySet()) {
            nodes.add(new Node(entry.getKey(), entry.getValue()));
        }

        // Print nodes
        clearConsole(1);
        System.out.println("Nodes:");
        for (Node node : nodes) {
            System.out.printf("%-10s%d\n", node.character + ":", node.frequency);
        }

        // Get tree
        Node tree = buildTree(nodes);

        // Get map of bit codes for all characters
        Map<Character, String> codesMap = getBitsRepresentations(tree, "");

        // Print bits
        clearConsole(1);
        System.out.println("Bits representations:");
        for (Map.Entry<Character, String> entry : codesMap.entrySet()) {
            System.out.printf("%-10s%s\n", entry.getKey() + ":", entry.getValue());
        }

        // Get final compressed code
        String compressed = "";
        for (Character character : text.toCharArray()) {
            compressed += codesMap.get(character);
        }

        // Print final code
        clearConsole(1);
        System.out.printf("%-10s%s\n", "Bits:", compressed);

        String path = getUserInput("Path: ");

        // Write compressed code and huffman tree
        Writer.writeCode(path + "compressed.txt", compressed);
        Writer.writeTree(path + "compressed.ser", tree);
    }

    /**
     * For each call it removes the first 2 nodes (left & right child nodes)
     * and adds a parent node (containing these child nodes).
     * Recalls itself until only one node (top node) is left.
     * @param nodes     a TreeSet type set of nodes
     * @return          top node containing child nodes (tree)
     */
    private Node buildTree(TreeSet<Node> nodes) {
        // Return remaining node containing tree of child nodes
        if (nodes.size() == 1) return nodes.first();

        Node leftChild = nodes.first();
        nodes.remove(leftChild);

        Node rightChild = nodes.first();
        nodes.remove(rightChild);

        Node parent = new Node(leftChild, rightChild);
        nodes.add(parent);

        // Recall method
        return buildTree(nodes);
    }

    /**
     * Get bits representations for all 'end points' (characters).
     * Recalls itself until an 'end point' (character) is reached.
     * @param node      node to generate bit code from
     * @param bits      the bits which represents a character
     * @return          a map containing character - bits pairs
     *                  (each bits representing a character)
     */
    private Map<Character, String> getBitsRepresentations(Node node, String bits) {
        Map<Character, String> codesMap = new HashMap<>();

        // If node has no children it's an end point (return it's character & bits representation)
        if (node.leftChild == null && node.rightChild == null) {
            codesMap.put(node.character, bits);
            return codesMap;
        }

        // (Else) get bits representations left & right children
        codesMap.putAll(getBitsRepresentations(node.leftChild, bits + "0"));
        codesMap.putAll(getBitsRepresentations(node.rightChild, bits + "1"));

        return codesMap;
    }

    private void expand() {
        // Get user input
        String path = getUserInput("path: ");

        // Read data
        Node tree = Reader.readTree(path + "compressed.ser");
        String code = Reader.readCode(path+ "compressed.txt");

        // Print compressed code
        clearConsole(1);
        System.out.printf("%-10s%s\n", "Bits:", code);

        // Loop through entire tree
        // and get character representation for bits
        int index = 0;
        while (index < code.length()) {
            Node tmp = tree;
            while (!tmp.isLeaf()) {
                if (code.charAt(index) == '1') tmp = tmp.rightChild;
                else tmp = tmp.leftChild;
                index++;
            }

            System.out.printf("%-10s%s\n", "Char:", tmp.character);
        }
    }

    /**
     * Console that calls {@code run()}
     *
     * @param args the command-line arguments
     */
    public static void main(String[] args) {
        Main main = new Main();
        main.run();
    }

    /**
     * Console loop
     */
    private void run() {
        boolean exit = false;
        while (!exit) {
            clearConsole(1);
            System.out.println("HUFFMAN compression - expanding");
            System.out.printf(FIELD_FORMAT, FIELD_OFFSET, "[0] Exit");
            System.out.printf(FIELD_FORMAT, FIELD_OFFSET, "[1] Compress");
            System.out.printf(FIELD_FORMAT, FIELD_OFFSET, "[2] Expand");
            String input = getUserInput("> ");

            switch (input) {
                case "0": exit = true;
                    break;
                case "1":
                    compress();
                    break;
                case "2":
                    expand();
                    break;
            }
        }
    }

    private String getUserInput(String what) {
        System.out.print(what);
        return scanner.next();
    }

    private void clearConsole(int lines) {
        for (int i = 0 ; i < lines; i++) {
            System.out.println("");
        }
    }
}
