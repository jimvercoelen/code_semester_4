package com.jim.util;

import com.jim.Main;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;


public class Reader {

    public static String readTxtFile(String absolutePath) {
        try {
            return String.join("\n", Files.readAllLines(Paths.get(absolutePath)));
        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
        }
        return null;
    }

    public static Main.Node readTree(String absolutePath) {
        try (BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(absolutePath))) {
            ObjectInputStream inputStream = new ObjectInputStream(bufferedInputStream);
            return (Main.Node) inputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
        }
        return null;
    }

    public static String readCode(String absolutePath) {
        // Variables
        String restBits = "";
        String bits = "";
        String digits = "0123456789ABCDEF";

        // Get and print context of file
        String context = readTxtFile(absolutePath);
        System.out.println("");
        System.out.printf("%10s%s\n", "Context:", context);

        // Subtract 'rest' bits from context if there are any!
        if (context.contains("/")) {
            restBits = context.substring(context.indexOf("/") + 1);
            context = context.substring(0, context.indexOf("/"));
        }

        // Get hex codes from context,
        // loop through al codes and convert hex to decimal to bits.
        String[] hexCodes = context.split("(?<=\\G..)");
        for (int i = 0; i < hexCodes.length; i++) {
            String hex = hexCodes[i];
            int val = 0;

            // Convert hex to decimal
            for (int j = 0; j < hex.length(); j++) {
                char c = hex.charAt(j);
                int d = digits.indexOf(c);
                val = 16 * val + d;
            }

            // Convert decimal to bits
            bits += Integer.toBinaryString(val);
        }

        // Add rest bits
        bits += restBits;

        return bits;
    }

}
