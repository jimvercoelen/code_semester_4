package com.jim.util;

import com.jim.Main;

import java.io.*;
import java.nio.charset.Charset;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Writer {

    public static void writeTree(String absolutePath, Main.Node tree) {
        try {
            FileOutputStream out = new FileOutputStream(new File(absolutePath));
            try (ObjectOutputStream writer = new ObjectOutputStream(new BufferedOutputStream(out))) {
                writer.writeObject(tree);
            }
        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
        }
    }

    public static void writeCode(String absolutePath, String code) {
        // Variables
        String hex = "";
        BitWriter bitWriter;

        // Split compressed code into bit-sets of 8 bits long,
        // and loop through all sets.
        String[] bitsArray = code.split("(?<=\\G........)");
        for (String bits : bitsArray) {
            // Check if bit-set is 8 bits long.
            // If this is the case, convert bits to bytes to hex.
            // Else add 'rest' bits to hex string with "/" as divider.
            if (bits.length() == 8) {

                // Convert bits to bytes
                bitWriter = new BitWriter();
                for (Character bit : bits.toCharArray()) bitWriter.writeBit(bit == '1');

                // Convert bytes to hex
                for (byte b : bitWriter.toArray()) hex += String.format("%02X", b);
            } else {
                hex += "/" + bits;
            }

        }

        // Print final hex (including rest bits)
        System.out.println("");
        System.out.printf("%-10s%s\n", "Hex:", hex);

        // Write file
        try {
            PrintWriter printWriterCode = new PrintWriter(absolutePath, "UTF-8");
            printWriterCode.println(hex);
            printWriterCode.close();
        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
        }
    }
}
