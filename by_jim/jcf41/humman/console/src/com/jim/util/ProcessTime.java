package com.jim.util;

public class ProcessTime {
    // Performance testing purpose only
    private static long begin, end, result;
    private static final String PROCESS_TIME_TEXT = "PROCESS TIME";
    private static final String STRING_FORMAT_COL_1 = "%-20s\n";
    private static final String STRING_FORMAT_COL_2 = "%-20s%s\n";

    public static void begin() {
        begin = System.nanoTime();
    }

    public static void end() {
        end = System.nanoTime();
    }

    public static void log(String title, String what, int amount) {
        result = end - begin;
        System.out.println();
        System.out.printf(STRING_FORMAT_COL_1, PROCESS_TIME_TEXT + " " + title);
        System.out.printf(STRING_FORMAT_COL_2, what + ":", amount);
        System.out.printf(STRING_FORMAT_COL_2, "Time:", result + " ms");
    }
}
