package woordenapplicatie.gui;

import java.net.URL;
import java.util.*;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;

public class WoordenController implements Initializable {

    private static List<String> SPLITTED_TEXT_LIST;

    private static final String DEFAULT_TEXT = "" +
            "Een, twee, drie, vier\n" +
            "Hoedje van, hoedje van\n" +
            "Een, twee, drie, vier\n" +
            "Hoedje van papier\n" +
            "\n" +
            "Heb je dan geen hoedje meer\n" +
            "Maak er één van bordpapier\n" +
            "Eén, twee, drie, vier\n" +
            "Hoedje van papier\n" +
            "\n" +
            "Een, twee, drie, vier\n" +
            "Hoedje van, hoedje van\n" +
            "Een, twee, drie, vier\n" +
            "Hoedje van papier\n" +
            "\n" +
            "En als het hoedje dan niet past\n" +
            "Zetten we 't in de glazenkas\n" +
            "Een, twee, drie, vier\n" +
            "Hoedje van papier";

    @FXML
    private TextArea taInput;

    @FXML
    private TextArea taOutput;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        taInput.setText(DEFAULT_TEXT);
        taOutput.setStyle("-fx-font-family: monospace");
        taInput.setEditable(false);
        taOutput.setEditable(false);

        SPLITTED_TEXT_LIST = Arrays.asList(DEFAULT_TEXT.replaceAll(",", "").split("\\s+"));
    }

    /* Usage of HashSet because this set only contains unique values */
    @FXML
    private void aantalAction(ActionEvent event) {
        Set<String> uniqueSet = new HashSet<>(SPLITTED_TEXT_LIST);

        int totalAmount = SPLITTED_TEXT_LIST.size();
        int uniqueAmount = uniqueSet.size();

        // Appending values on UI
        taOutput.clear();
        taOutput.appendText(String.format("%-30s%d\n", "Totaal aantal woorden:", totalAmount));
        taOutput.appendText(String.format("%-30s%d\n", "Totaal aantal unieke woorden:", uniqueAmount));
    }

    /* Usage of TreeSet because this set type automatically sorts the values */
    @FXML
    private void sorteerAction(ActionEvent event) {
        Set<String> set = new TreeSet<>();
        set.addAll(SPLITTED_TEXT_LIST);

        // Appending values on UI
        taOutput.clear();
        for (String word: set) {
            taOutput.appendText(word + "\n");
        }
    }

    /*
     * Usage of HashSet because this set type contains unique values,
     * usage of Hashmap because this map type can contain key and value pairs <Word, Frequency>,
     * usage of LinkedHashMap because this map type iterates through the entries in the ordered hey where put in.
     */
    @FXML
    private void frequentieAction(ActionEvent event) {
        // Getting frequency for each word
        Map<String, Integer> frequencyMap = new HashMap<>(SPLITTED_TEXT_LIST.size());
        for (String word: SPLITTED_TEXT_LIST) {
            frequencyMap.put(word, frequencyMap.getOrDefault(word, 0) + 1);
        }

        // Sorting 'unsortable' hashmap (hacky hacky)
        List<Map.Entry<String, Integer>> unsortedList = new ArrayList<>(frequencyMap.entrySet());
        Collections.sort(unsortedList, (o1, o2) -> (o1.getValue()).compareTo(o2.getValue()));
        Map<String, Integer> sortedMap = new LinkedHashMap<>();
        for (Map.Entry<String, Integer> entry : unsortedList) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        // Appending values on UI
        taOutput.clear();
        for (Map.Entry<String, Integer> entry : sortedMap.entrySet()) {
            taOutput.appendText(String.format("%-12s%s\n", entry.getKey(), entry.getValue().toString()));
        }
    }

    /*
     * Usage of TreeMap because this map type contains key - value pairs and automatically sorts on key.
     * This allows us to create a list of indices for each word.
     */
    @FXML
    private void concordatieAction(ActionEvent event) {
        // Getting indices for each word
        Map<String, List<Integer>> treeMap = new TreeMap<>();
        for (int i = 0; i < SPLITTED_TEXT_LIST.size(); i++) {
            String word = SPLITTED_TEXT_LIST.get(i);

            if (!treeMap.containsKey(word)) {
                treeMap.put(word, new ArrayList<>());
            }

            treeMap.get(word).add(i);
        }

        // Appending values on UI
        taOutput.clear();
        for (Map.Entry<String, List<Integer>> entry : treeMap.entrySet()) {
            taOutput.appendText(String.format("%-12s%s\n", entry.getKey(), entry.getValue().toString()));
        }
    }

}
