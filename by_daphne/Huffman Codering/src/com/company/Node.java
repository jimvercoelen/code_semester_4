package com.company;

/**
 * Created by Daphne on 17-9-2016.
 */
public class Node implements Comparable<Node> {
    public char character;
    public int frequency;
    public Node leftChild;
    public Node rightChild;

    Node(char character, int frequency)
    {
        this.character = character;
        this.frequency = frequency;
    }

    Node(Node leftChild, Node rightChild, int frequency)
    {
        character = 42;
        this.leftChild = leftChild;
        this.rightChild = rightChild;
        this.frequency = frequency;
    }

    @Override
    public int compareTo(Node o)
    {
        return this.frequency < o.frequency ? + 1
                : this.frequency > o.frequency ? - 1
                : ((Character) this.character).compareTo(((Character) o.character));
    }
}
