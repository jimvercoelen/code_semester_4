package com.company;

import java.util.*;

/**
 * Created by Daphne on 17-9-2016.
 */
public class Actions {

    private List<Node> nodes = new ArrayList();

    public void inputToNodes(String text) {
        HashMap<Character, Integer> charWithFreq = new HashMap<>();
        for (char character : text.toCharArray())
        {
            charWithFreq.put(character, charWithFreq.getOrDefault(character, 0) + 1);
        }

        for (Map.Entry<Character, Integer> entry : charWithFreq.entrySet())
        {
            nodes.add(new Node(entry.getKey(), entry.getValue()));
        }

        Collections.sort(nodes);
        System.out.println("Input: " + text + "\n");
        System.out.println("Letter  Frequency");
        for (Node node : nodes)
        {
            System.out.println("  "  + node.character + "        " + node.frequency);
        }

        nodesToTree();
    }

    private void nodesToTree()
    {
        Comparator<Node> comparator = new NodeComparator();
        PriorityQueue<Node> priorityQueue = new PriorityQueue<>(10, comparator);

        Node node = new Node(nodes.get(nodes.size() - 1), nodes.get((nodes.size()) - 2), nodes.get(nodes.size() - 1).frequency + nodes.get((nodes.size()) - 2).frequency);
        nodes.remove(nodes.get(nodes.size() - 1));
        nodes.remove(nodes.get(nodes.size() - 1));
        priorityQueue.add(node);

        while (!nodes.isEmpty())
        {
            Node newNode = new Node(nodes.get(nodes.size() - 1), node, nodes.get(nodes.size() - 1).frequency + node.frequency);
            nodes.remove(nodes.get(nodes.size() - 1));
            node = newNode;
            priorityQueue.add(node);
        }

        // Polling is nodig omdat de lijst dan pas gesorteerd ('geordered') wordt. Hierbij wordt de head verwijderd, dus die sla ik even op in headnote. Zie Stack Overflow-link op Slack voor uitleg.
        Node headNode = priorityQueue.poll();
        System.out.print("\nNode: " + headNode.character + ":" + headNode.frequency + "\n   " + headNode.leftChild.character + ":" + headNode.leftChild.frequency + " - " + headNode.rightChild.character + ":" + headNode.rightChild.frequency + "\n");

        for (Node n : priorityQueue)
        {
            System.out.print("\nNode: " + n.character + ":" + n.frequency + "\n   " + n.leftChild.character + ":" + n.leftChild.frequency + " - " + n.rightChild.character + ":" + n.rightChild.frequency + "\n");
        }
    }
}
