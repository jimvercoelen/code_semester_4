package com.company;

import java.util.Comparator;

/**
 * Created by Daphne on 17-9-2016.
 */
public class NodeComparator implements Comparator<Node>{

    @Override
    public int compare(Node o1, Node o2) {
        return Integer.compare(o2.frequency, o1.frequency);
    }
}
