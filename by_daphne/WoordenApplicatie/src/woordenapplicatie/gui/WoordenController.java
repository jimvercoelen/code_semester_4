package woordenapplicatie.gui;

import java.net.URL;
import java.util.*;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;

public class WoordenController implements Initializable {

    private static String[] SPLITTED_TEXT;

    private static final String DEFAULT_TEXT = "" +
            "Een, twee, drie, vier\n" +
            "Hoedje van, hoedje van\n" +
            "Een, twee, drie, vier\n" +
            "Hoedje van papier\n" +
            "\n" +
            "Heb je dan geen hoedje meer\n" +
            "Maak er één van bordpapier\n" +
            "Eén, twee, drie, vier\n" +
            "Hoedje van papier\n" +
            "\n" +
            "Een, twee, drie, vier\n" +
            "Hoedje van, hoedje van\n" +
            "Een, twee, drie, vier\n" +
            "Hoedje van papier\n" +
            "\n" +
            "En als het hoedje dan niet past\n" +
            "Zetten we 't in de glazenkas\n" +
            "Een, twee, drie, vier\n" +
            "Hoedje van papier";

    @FXML
    private TextArea taInput;

    @FXML
    private TextArea taOutput;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        taInput.setText(DEFAULT_TEXT);
        String defaultText = DEFAULT_TEXT.toLowerCase();
        taOutput.setStyle("-fx-font-family: monospace");
        SPLITTED_TEXT = defaultText.replaceAll(",", "").split("\\s+");
        taInput.setEditable(false);
        taOutput.setEditable(false);
    }

    @FXML
    private void aantalAction(ActionEvent event) {
        /* Usage of HashSet because this set only contains unique values */
        Set<String> uniqueSet = new HashSet<>();
        Collections.addAll(uniqueSet, SPLITTED_TEXT);

        int totalAmount = SPLITTED_TEXT.length;
        int uniqueAmount = uniqueSet.size();

        taOutput.clear();
        taOutput.appendText(String.format("%-30s%d\n", "Totaal aantal woorden:", totalAmount));
        taOutput.appendText(String.format("%-30s%d\n", "Totaal aantal unieke woorden:", uniqueAmount));
    }

    @FXML
    private void sorteerAction(ActionEvent event) {
        /* Usage of TreeSet because this set type automatically sorts the values */
        TreeSet<String> list = new TreeSet<>();
        for (int i = 0; i < SPLITTED_TEXT.length; i++) {
            String word = SPLITTED_TEXT[i];
            list.add(word);
        }

        taOutput.clear();
        for (String word: list) {
            taOutput.appendText(word + "\n");
        }
    }

    @FXML
    private void frequentieAction(ActionEvent event) {
        /* Usage of ArrayList because this list has contains every word (duplicates included) in unsorted order and
         * usage of HashSet because this set only contains unique values. Usage of Map because you can determine
         * the key and value here.
         */
        Set<String> uniqueSet = new HashSet<>();
        Collections.addAll(uniqueSet, SPLITTED_TEXT);
        ArrayList allWords = new ArrayList();
        Collections.addAll(allWords, SPLITTED_TEXT);
        HashMap<String, Integer> map = new HashMap();

        for (String word: uniqueSet) {
            int frequency = Collections.frequency(allWords, word);
            map.put(word.toString(), frequency);
        }

        Map<String, Integer> sorted = sortByValue(map);

        taOutput.clear();
        for (String word : sorted.keySet())
        {
            taOutput.appendText(String.format("%-12s%d\n", word, map.get(word)));
        }
    }

    private static Map<String, Integer> sortByValue(Map<String, Integer> unsortMap) {

        // 1. Convert Map to List of Map
        List<Map.Entry<String, Integer>> list =
                new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

        // 2. Sort list with Collections.sort(), provide a custom Comparator
        //    Try switch the o1 o2 position for a different order
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }


    @FXML
    private void concordatieAction(ActionEvent event) {
        /*
         * Usage of TreeMap because this map type contains key - value pairs and automatically
         * sorts all keys.
         */
        TreeMap<String, List<Integer>> treeMap = new TreeMap<>();
        for (int i = 0; i < SPLITTED_TEXT.length; i++) {
            String word = SPLITTED_TEXT[i];
            if (!treeMap.containsKey(word)) {
                treeMap.put(word, new ArrayList<>());
            }

            treeMap.get(word).add(i);
        }

        taOutput.clear();
        for (Map.Entry<String, List<Integer>> entry : treeMap.entrySet()) {
            taOutput.appendText(String.format("%-12s%s\n", entry.getKey(), entry.getValue().toString()));
        }
    }
}
