package woordenapplicatie.gui;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import java.net.URL;
import java.util.*;

import com.sun.corba.se.impl.orbutil.ObjectWriter;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * FXML Controller class
 *
 * @author frankcoenen
 */
public class WoordenController implements Initializable
{
    private String input;
    private static final String DEFAULT_TEXT =   "Een, twee, drie, vier\n" +
                                                "Hoedje van, hoedje van\n" +
                                                "Een, twee, drie, vier\n" +
                                                "Hoedje van papier\n" +
                                                "\n" +
                                                "Heb je dan geen hoedje meer\n" +
                                                "Maak er één van bordpapier\n" +
                                                "Eén, twee, drie, vier\n" +
                                                "Hoedje van papier\n" +
                                                "\n" +
                                                "Een, twee, drie, vier\n" +
                                                "Hoedje van, hoedje van\n" +
                                                "Een, twee, drie, vier\n" +
                                                "Hoedje van papier\n" +
                                                "\n" +
                                                "En als het hoedje dan niet past\n" +
                                                "Zetten we 't in de glazenkas\n" +
                                                "Een, twee, drie, vier\n" +
                                                "Hoedje van papier";
    
    @FXML
    private Button btAantal;
    @FXML
    private TextArea taInput;
    @FXML
    private Button btSorteer;
    @FXML
    private Button btFrequentie;
    @FXML
    private Button btConcordantie;
    @FXML
    private TextArea taOutput;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        getText();
    }

    private void getText()
    {
        if(taInput.getText().equals(""))
        {
            taInput.setText(DEFAULT_TEXT);
            String inputAll = taInput.getText();
            input = inputAll.toLowerCase();
        }
        else
        {
            String inputAll = taInput.getText();
            input = inputAll.toLowerCase();
        }
    }
    @FXML
    private void aantalAction(ActionEvent event)
    {
        //Create string and put text in the collection
        getText();
        String[] text = input.split("\\s+");
        HashSet set = new HashSet();
        Collections.addAll(set, text);

        //Display the results
        taOutput.clear();
        taOutput.appendText("Tataal aantal woorden: " + text.length + System.lineSeparator());
        taOutput.appendText("Aantal verschillende woorden: " + set.size());

        //Ik hie hier gekozen voor een Hashset, een hashset is uniek.
        //Tevens heb ik voor een set gekozen omdat ik geen keypairs nodig heb en dus geen hashmap.
    }

    @FXML
    private void sorteerAction(ActionEvent event)
    {
        //Create string and put text in the collection
        getText();
        String[] text = input.split("\\s+");
        HashSet set = new HashSet();
        Collections.addAll(set, text);

        //Put them in an arraylist, now it is possible to sort
        ArrayList sorted = new ArrayList();
        for(Object word : set)
        {
            sorted.add(word);
        }

        //Sort the arraylist backwards
        Collections.sort(sorted, Collections.reverseOrder());


        //Display all the words
        taOutput.clear();
        for(Object word : sorted)
        {
            taOutput.appendText(word + System.lineSeparator());
        }

        //Hier heb ik gebruik gemaakt van een hashset om alle unieke woorden te krijgen,
        //dit heb ik omgezet naar een arraylist om te sorteren en daarna af te drukken.
    }

    @FXML
    private void frequentieAction(ActionEvent event)
    {
        //Create string and put text in the collection
        getText();
        String[] text = input.split("\\s+");
        HashSet set = new HashSet();
        Collections.addAll(set, text);

        //Create a list with all words
        ArrayList allWords = new ArrayList();
        Collections.addAll(allWords, text);

        //Count all words and put them in a hashmap
        HashMap<String, Integer> map = new HashMap();
        for(Object word : set)
        {
            int frequency = Collections.frequency(allWords, word);
            map.put(word.toString(), frequency);
        }

        //Sort the hashmap
        Map<String, Integer> sorted = sortByValue(map);

        //Print the values
        taOutput.clear();
        for(String word : sorted.keySet())
        {
            taOutput.appendText(word + ": " +  map.get(word) + System.lineSeparator());
        }

        //Er worden 2 lijsten gemaakt, 1 hashset waar alle unieke woorden inzitten en 1 arraylist met alle woorden
        //Daarna heb ik een hashmap aangemaakt met het woord als key en de frequentie als value, hier wordt door
        //de lijst gelopen die uniek is en worden alle woorden geteld en in de hashmap opgeslagen.
        //Als laatste wordt de map gesorteerd en afgedrukt
    }

    @FXML
    private void concordatieAction(ActionEvent event)
    {
        //Basics
        getText();
        String text = input;
        String[] words = input.split("\\s+");

        //List with all unique words
        HashSet allUniqueWords = new HashSet();
        Collections.addAll(allUniqueWords, words);

        //Split sentences
        List<String> sentences = new LinkedList();
        String sentence = "";

        for (int i = 0; i < text.length() ; i++)
//        for (char c : text.chars())
        {
            String c = Character.toString(text.charAt(i));

            if (c.equals("\n"))
            {
                if (sentence != "")
                {
                    sentences.add(sentence);
                }
                sentence = "";
            }
            else
            {
                sentence = sentence + c;
                if (i == text.length() - 1)
                {
                    sentences.add(sentence);
                }
            }
        }

        //Treemap with all unique words and places
        Map<String, ArrayList<Integer>> result = new TreeMap();
        for(Object o : allUniqueWords)
        {
            result.put(o.toString(), new ArrayList<>());
        }

        //Loop through all unique words
        for(Object uniqueWord : allUniqueWords)
        {
            //Create a list for the positions of each unique word
            ArrayList<Integer> positions = new ArrayList<>();
            int line = 0;

            //Loop through all words in a sentence
            for(String s : sentences)
            {
                //Each new sentence has a new line number
                line++;

                //Split all words and put them in a list
                String[] w = s.split("\\s+");
                HashSet allWordsInSentence = new HashSet();
                Collections.addAll(allWordsInSentence, w);

                //Loop through all words in the sentence
                for(Object o : allWordsInSentence)
                {
                    //Check if the unique word equals the word in te sentence
                    if(o.toString().equals(uniqueWord.toString()))
                    {
                        //If accepted, search fot the key in the hashmap
                        for (String key : result.keySet())
                        {
                            if (key.equals(o.toString()))
                            {
                                //Add the line number in the list and add the updated list in the hashmap
                                positions.add(line);
                                result.put(key, positions);
                            }
                        }
                    }
                }
            }
        }

        //Print the result
        taOutput.clear();
        for (Map.Entry<String,ArrayList<Integer>> entry : result.entrySet())
        {
            String key = entry.getKey();
            String value = entry.getValue().toString();

            taOutput.appendText(key + " : " + value + "\n");
        }

        //Deze methode is heel uitgebreid. Hier heb ik een heel systeem bedacht om alles te kunnen tellen:
        //Eerst worden de zinnen gemaakt en in een lijst gezet. Dit doe ik ook voor alle unieke woorden.
        //Daarna worden er door ieder uniek woord gelopen. Zodra dit gebeurd wordt er door iedere zin gelopen,
        //hier worden alle woorden van een zin in een nieuwe lijst gestopt en dan wordt er hier doorheen gelopen.
        //Als het unieke woord en het woord in de zin overeenkomen wordt deze toegevoegd op de juiste plaats
        //in de treemap, tevens wordt het regelnummer toegevoegd. Omdat het een treemap is wordt alles automatisch gesorteerd.
        //Hier zijn een aantal verschillende lijsten gebruikt, dit heb ik voor de performance en de eigenschappen gedaan.
    }

    //Method to sort a map with a String as index and an integer as value
    private Map<String, Integer> sortByValue(Map<String, Integer> unsortMap)
    {
        // 1. Convert Map to List of Map
        List<Map.Entry<String, Integer>> list =
                new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

        // 2. Sort list with Collections.sort(), provide a custom Comparator
        //    Try switch the o1 o2 position for a different order
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : list)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }
}
